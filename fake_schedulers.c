#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include "fake_schedulers.h"

void schedRR(FakeOS* os, void* args_){
  // aw: gets arguments
  SchedRRArgs* args=(SchedRRArgs*)args_;

  // look for the first process in ready
  // if none, return
  if (! os->ready.first)
    return;

  // aw: get first element of ready list and puts it in running
  // aw: if it has affinity with this processor
  FakePCB* pcb=(FakePCB*) List_popFront(&os->ready);
  int loop = pcb->pid;
  while(pcb->affinity!=-1 && pcb->affinity!=args->processor){
    List_pushBack(&os->ready, (ListItem*)pcb);
    if(os->ready.size==1) return;
    pcb=(FakePCB*) List_popFront(&os->ready);
    if(pcb->pid == loop) return;
  }
  os->running[args->processor]=pcb;

  // aw: get first event and assert that it is indeed a CPU type event
  assert(pcb->events.first);
  ProcessEvent* e = (ProcessEvent*)pcb->events.first;
  assert(e->type==CPU);
};

void schedSRJF(FakeOS* os, void* args_){
  // aw: gets arguments
  SchedRRArgs* args=(SchedRRArgs*)args_;

  // look for the first process in ready
  // if none, return
  if (! os->ready.first)
    return;

  int shortest = INT_MAX;
  FakePCB* to_run = 0;

  ListItem* aux=os->ready.first;
  while (aux){
    FakePCB* pcb =(FakePCB*)aux;
    aux=aux->next;
    ProcessEvent* e=(ProcessEvent*) pcb->events.first;
    if(e->type==CPU
	&& e->duration < shortest
	&& (pcb->affinity==-1 || pcb->affinity==args->processor)){
      shortest = e->duration;
      to_run = pcb;
    }
  }

  if(!to_run) return;
  // aw: remove element chosen from ready list and puts it in running
  List_detach(&(os->ready), (ListItem*)to_run);
  os->running[args->processor]=to_run;
};

void schedPriority(FakeOS* os, void* args_){
  // aw: gets arguments
  SchedRRArgs* args=(SchedRRArgs*)args_;

  // look for the first process in ready
  // if none, return
  if (! os->ready.first)
    return;

  // aw: checks for highest priority (pa < pb: pa has highest priority)
  int priority = INT_MAX;
  FakePCB* to_run = 0;

  ListItem* aux=os->ready.first;
  while (aux){
    FakePCB* pcb =(FakePCB*)aux;
    aux=aux->next;
    ProcessEvent* e=(ProcessEvent*) pcb->events.first;
    if(e->type==CPU 
	&& pcb->priority < priority
	&& (pcb->affinity==-1 || pcb->affinity==args->processor)){

      // aw: keep priority =>0
      priority = pcb->priority;
      if(to_run && to_run->priority>0)
      	to_run->priority--;
      to_run = pcb;
    }
    else if(e->type==CPU){
      if(pcb && pcb->priority>0)
      	pcb->priority--;
    }
  }
  if(!to_run) return;

  // aw: remove element chosen from ready list and puts it in running
  // aw: also, resets priority
  List_detach(&(os->ready), (ListItem*)to_run);
  os->running[args->processor]=to_run;
  to_run->priority=to_run->reset;
};

// aw: loads the scheduling policies from a given file
// aw: default "example/config.txt" otherwise
int loadConfig(const char* filename, FakeOS* os){
  //aw: open filename if given, open config.txt otherwise
  FILE* f;
  if(filename) f=fopen(filename, "r");
  else f=fopen("example/config.txt", "r"); 

  if (! f)
    return -1;
  //aw: getting ready to read
  char *buffer=NULL;
  size_t line_length=0;
  int processors=-1;
  int preemptive=1;
  int num_tokens=0;
  PolicyType type=0;
  int i=0;
  while (getline(&buffer, &line_length, f) >0){
    // got line in buf
    if(buffer[0]=='#') goto next_round;

    num_tokens=sscanf(buffer, "PREEMPTIVE: %d", &preemptive);
    if (num_tokens==1 && os->preemptive==-1){
      os->preemptive=preemptive;
      goto next_round;
    }

    num_tokens=sscanf(buffer, "PROCESSORS: %d", &processors);
    if (num_tokens==1 && os->processors==-1){
      assert(processors>0 && "invalid number of processors");
      os->processors=processors;
      os->running = (FakePCB**)malloc(sizeof(FakePCB*)*processors);
      os->schedule_fn = (ScheduleFn*)malloc(sizeof(ScheduleFn)*processors);
      os->cpu_utilization = (float*)malloc(sizeof(float)*processors);
      memset(os->running, 0, sizeof(FakePCB*)*processors);
      memset(os->schedule_fn, 0, sizeof(ScheduleFn)*processors);
      memset(os->cpu_utilization, 0, sizeof(float)*processors);
      goto next_round;
    }

    if(i>=os->processors) break; 
    num_tokens=sscanf(buffer, "SCHEDULING: %d", (int*)(&type));
    if(num_tokens==1){
      switch (type){
        case RR:
          printf("processor %d will use \"Round Robin\" policy\n", i);
          os->schedule_fn[i++]=schedRR;
          break;
	case SRJF:
	  printf("processor %d will use \"Shortest Job First\" policy\n", i);
	  os->schedule_fn[i++]=schedSRJF;
	  break;
	case Priority:
	  printf("processor %d will use \"Priority\" policy\n", i);
	  os->schedule_fn[i++]=schedPriority;
	  break;
        default:
          printf("scheduling policy %d doesn't exists, will use \"Round Robin\" policy\n", type);
	  os->schedule_fn[i++]=schedRR;
      }
    }
    next_round:;
  }

  if(os->processors<1){
    os->processors=1;
    os->running = (FakePCB**)malloc(sizeof(FakePCB*)*processors);
    os->schedule_fn = (ScheduleFn*)malloc(sizeof(ScheduleFn)*processors);
    os->cpu_utilization = (float*)malloc(sizeof(float)*processors);
    memset(os->running, 0, sizeof(FakePCB*)*processors);
    memset(os->schedule_fn, 0, sizeof(ScheduleFn)*processors);
    memset(os->cpu_utilization, 0, sizeof(float)*processors);
  }
  if(os->preemptive<0)
    os->preemptive=1;
  if(!os->schedule_fn[0])
    os->schedule_fn[0]=schedRR;
  if (buffer)
    free(buffer);
  fclose(f);
  return processors;
}

