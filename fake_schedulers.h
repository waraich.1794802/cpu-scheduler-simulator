#pragma once
#include "fake_os.h"

typedef enum {RR=0, SRJF=1, Priority=2} PolicyType;

// aw: scheduler args
typedef struct {
  int processor;
  //aw: not used, but can be in custom algorithms:
  int quantum;
  int preemptive;
} SchedRRArgs;

// aw: scheduling policies
void schedRR(FakeOS* os, void* args_);
void schedSRJF(FakeOS* os, void* args_);
void schedPriority(FakeOS* os, void* args_);

// aw: load policies from file
int loadConfig(const char* filename, FakeOS* os);
