#include "fake_process.h"
#include "linked_list.h"
#pragma once


typedef struct {
  ListItem list;
  int pid;
  int priority;
  int reset;
  int affinity;
  // measures
  int turnaround_time;
  int waiting_time;
  int response_time;
  ListHead events;
} FakePCB;

struct FakeOS;
typedef void (*ScheduleFn)(struct FakeOS* os, void* args);

typedef struct FakeOS{
  FakePCB** running;
  ScheduleFn* schedule_fn;

  ListHead ready;
  ListHead waiting;
  ListHead defunct;
  int timer;
  void* schedule_args;

  int global_pid;
  int preemptive;
  int processors;
  int running_current;

  //aw: measures
  float* cpu_utilization;
  float throughput;

  ListHead processes;
} FakeOS;

void FakeOS_init(FakeOS* os);
void FakeOS_simStep(FakeOS* os);
void FakeOS_destroy(FakeOS* os);
