// Should take in input processes, number of processors, scheduling
// Outputs internal situation and parameters

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "fake_os.h"
#include "fake_schedulers.h"

FakeOS os;

int main(int argc, char** argv) {
  FakeOS_init(&os);
  SchedRRArgs srr_args;
  srr_args.quantum=5;
  os.schedule_args=&srr_args;

  for (int i=1; i<argc; ++i){
    if (!strcmp(argv[i], "-c")){
      if(loadConfig(argv[i+1], &os) < 1){
        printf("Config file couldn't be found or read\n");
        return EXIT_FAILURE;
      }
      printf("Number of processors: %d\n", os.processors);
      i++;
      continue;
    }
    FakeProcess new_process;
    int num_events=FakeProcess_load(&new_process, argv[i]);
    printf("loading [%s], id: %d, events:%d\n",
           argv[i], new_process.pid, num_events);
    if (num_events) {
      os.throughput++;
      FakeProcess* new_process_ptr=(FakeProcess*)malloc(sizeof(FakeProcess));
      *new_process_ptr=new_process;
      List_pushBack(&os.processes, (ListItem*)new_process_ptr);
    }
  }

  if(os.processors<1){
    if(loadConfig(0, &os) < 1){
      printf("Config file couldn't be found or read\n");
      return EXIT_FAILURE;
    }
    printf("Number of processors: %d\n", os.processors);
  }

  srr_args.preemptive = os.preemptive;  

  printf("num processes in queue %d\n", os.processes.size);
  while((os.running_current
        || os.ready.first
        || os.waiting.first
        || os.processes.first
        )){
    FakeOS_simStep(&os);
  }
  FakeOS_destroy(&os);
}
