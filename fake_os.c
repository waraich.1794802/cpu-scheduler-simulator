#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "fake_os.h"

void FakeOS_init(FakeOS* os) {
  List_init(&os->ready);
  List_init(&os->waiting);
  List_init(&os->defunct);
  List_init(&os->processes);
  os->timer=0;
  os->global_pid = 1;
  os->preemptive = -1;
  os->processors = -1;
  os->schedule_fn=0;
  os->running = 0;
  os->throughput = 0;
}

void FakeOS_createProcess(FakeOS* os, FakeProcess* p) {
  // sanity check
  assert(p->arrival_time==os->timer && "time mismatch in creation");
  assert(p->pid>=0 && "process file not valid");
  // aw: assign pid and check if affinity is a valid value
  // aw: if not then assign -1 (can run on any cpu)
  printf("\tprocess %d has pid %d\n", p->pid, os->global_pid);
  p->pid = os->global_pid++;
  if(p->affinity >= os->processors) p->affinity = -1;

  // make the pcb
  FakePCB* new_pcb=(FakePCB*) malloc(sizeof(FakePCB));
  new_pcb->list.next=new_pcb->list.prev=0;
  new_pcb->pid=p->pid;
  new_pcb->priority=p->priority;
  new_pcb->reset=p->priority;
  new_pcb->affinity=p->affinity;
  new_pcb->turnaround_time=p->arrival_time;
  new_pcb->response_time=-1;
  new_pcb->events=p->events;

  assert(new_pcb->events.first && "process without events");

  // depending on the type of the first event
  // we put the process either in ready or in waiting
  ProcessEvent* e=(ProcessEvent*)new_pcb->events.first;
  switch(e->type){
  case CPU:
    List_pushBack(&os->ready, (ListItem*) new_pcb);
    break;
  case IO:
    List_pushBack(&os->waiting, (ListItem*) new_pcb);
    break;
  default:
    assert(0 && "illegal resource");
    ;
  }
}




void FakeOS_simStep(FakeOS* os){
  
  printf("************** TIME: %08d **************\n", os->timer);

  //scan process waiting to be started
  //and create all processes starting now
  ListItem* aux=os->processes.first;
  while (aux){
    FakeProcess* proc=(FakeProcess*)aux;
    FakeProcess* new_process=0;
    if (proc->arrival_time==os->timer){
      new_process=proc;
    }
    aux=aux->next;
    if (new_process) {
      printf("\tcreate process:%d\n", new_process->pid);
      new_process=(FakeProcess*)List_detach(&os->processes, (ListItem*)new_process);
      FakeOS_createProcess(os, new_process);
      free(new_process);
    }
  }

  // scan waiting list, and put in ready all items whose event terminates
  aux=os->waiting.first;
    printf("\twaiting:");
    while(aux) {
      FakePCB* pcb=(FakePCB*)aux;
      aux=aux->next;
      ProcessEvent* e=(ProcessEvent*) pcb->events.first;
      printf("[%d, ", pcb->pid);
      assert(e->type==IO);
      e->duration--;
      printf("%d]",e->duration);
      if (e->duration==0){
        printf(" end burst");
        List_popFront(&pcb->events);
        free(e);
        List_detach(&os->waiting, (ListItem*)pcb);
        if (! pcb->events.first) {
          // kill process
          printf(", end process");
	  List_pushBack(&os->defunct, (ListItem*)pcb);
          pcb->turnaround_time = os->timer - pcb->turnaround_time;
        } else {
          //handle next event
          e=(ProcessEvent*) pcb->events.first;
          switch (e->type){
          case CPU:
            printf(", moved to ready");
            List_pushBack(&os->ready, (ListItem*) pcb);
            break;
          case IO:
            printf(", moved to waiting");
            List_pushBack(&os->waiting, (ListItem*) pcb);
            break;
          }
        }
	if(aux) printf("\n\t\t");
      }
    }
    printf("\n");

  // aw: print ready list
  aux=os->ready.first;
  printf("\tready:");
  while(aux){
    FakePCB* pcb=(FakePCB*)aux;
    aux=aux->next;
    ProcessEvent* e=(ProcessEvent*) pcb->events.first;
    printf("[%d, %d, %d, %d]", pcb->pid, pcb->priority, pcb->affinity, e->duration);
    pcb->waiting_time+=1;
  }
  printf("\n");

  // aw: for every processor
  // decrement the duration of every running event
  // if event over, destroy event
  // and reschedule process
  // if last event, destroy running
  printf("\trunning:");
  for(int i=0; i<os->processors; i++){
    FakePCB* running=os->running[i];
    if (running) {
      printf("[%d, ", running->pid);
      ProcessEvent* e=(ProcessEvent*) running->events.first;
      assert(e->type==CPU);
      e->duration--;
      printf("%d]",e->duration);
      if (e->duration==0){
        printf(" end burst");
        List_popFront(&running->events);
        free(e);
        if (! running->events.first) {
	  printf(", end process");
	  List_pushBack(&os->defunct, (ListItem*)running);
          running->turnaround_time = os->timer - running->turnaround_time;
        } else {
          e=(ProcessEvent*) running->events.first;
          switch (e->type){
          case CPU:
            printf(", moved to ready");
            List_pushBack(&os->ready, (ListItem*) running);
            break;
          case IO:
            printf(", moved to waiting");
            List_pushBack(&os->waiting, (ListItem*) running);
            break;
          }
        }
	if(i+1!=os->processors) printf("\n\t\t");
        os->running[i] = 0;
      }
    }
    else printf("[-1]");
  }
  printf("\n");

  // aw: for each processor call assigned schedule, if defined
  // aw: otherwise do the same as processor 0
  for(int i=0; i<os->processors; i++){
    if(!os->running[i]){
      *((int*)(os->schedule_args)) = i;
      if(os->schedule_fn[i]){
        (*os->schedule_fn[i])(os, os->schedule_args);
      }
      else{
        (*os->schedule_fn[0])(os, os->schedule_args);
      }
      if(os->running[i]&&os->running[i]->response_time<0)
	os->running[i]->response_time=os->timer-os->running[i]->turnaround_time;
      // aw: splits the first event of the running process
      // aw: to simulate preemption
      if(os->running[i]&&os->preemptive){
	ProcessEvent* ce=(ProcessEvent*) os->running[i]->events.first;
        int quantum = *((int*)os->schedule_args+1);
	if (ce->duration>quantum) {
    	  ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
    	  qe->list.prev=qe->list.next=0;
    	  qe->type=CPU;
    	  qe->duration=quantum;
    	  ce->duration-=quantum;
    	  List_pushFront(&(os->running[i]->events), (ListItem*)qe);
  	}
      }
    }
   
  } 

  // aw: check currently running processes
  os->running_current = 0;
  for(int i=0; i<os->processors; i++){
    if(os->running[i]){
      os->running_current++;
      os->cpu_utilization[i]+=1;
    }
  }

  ++os->timer;
}

void FakeOS_destroy(FakeOS* os) {
  float cpu_average = 0;
  float turnaround_avg = 0;
  float waiting_avg = 0;
  float response_avg = 0;
  int n = os->defunct.size;

  for(int i=0; i<os->processors; i++){
    printf("CPU %d: %f\n",i,os->cpu_utilization[i]/os->timer);
    cpu_average += os->cpu_utilization[i]/os->timer;
  }
  
  ListItem* aux = os->defunct.first;
  while(aux){
    FakePCB* pcb = (FakePCB*)aux;
    aux = aux->next;
    printf("Process %d:\n", pcb->pid);
    printf("Turnaround time: %d \tWaiting time: %d \tResponse time: %d\n", pcb->turnaround_time, pcb->waiting_time, pcb->response_time);
    turnaround_avg += pcb->turnaround_time;
    waiting_avg += pcb->waiting_time;
    response_avg += pcb->response_time;
    free(pcb);
  }
  
  printf("\nAverage CPU utilization: %f\n", cpu_average/os->processors);
  printf("Average Turnaround Time: %f\nAverage Waiting Time: %f\nAverage Response Time: %f\n", turnaround_avg/n, waiting_avg/n, response_avg/n);
  printf("Throughput: %f\n", os->throughput/os->timer);

  free(os->running);
  free(os->schedule_fn);
  free(os->cpu_utilization);
}
