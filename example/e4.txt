processor 0 will use "Shortest Job First" policy
Number of processors: 1
loading [example/p4.txt], id: 4, events:1
loading [example/p5.txt], id: 5, events:1
loading [example/p6.txt], id: 6, events:1
num processes in queue 3
************** TIME: 00000000 **************
	create process:4
	process 4 has pid 1
	waiting:
	ready:[1, 0, 0, 11]
	running:[-1]
************** TIME: 00000001 **************
	create process:5
	process 5 has pid 2
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 4]
************** TIME: 00000002 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 3]
************** TIME: 00000003 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 2]
************** TIME: 00000004 **************
	create process:6
	process 6 has pid 3
	waiting:
	ready:[2, 0, 0, 9][3, 0, 0, 2]
	running:[1, 1]
************** TIME: 00000005 **************
	waiting:
	ready:[2, 0, 0, 9][3, 0, 0, 2]
	running:[1, 0] end burst, moved to ready
************** TIME: 00000006 **************
	waiting:
	ready:[2, 0, 0, 9][1, 0, 0, 6]
	running:[3, 1]
************** TIME: 00000007 **************
	waiting:
	ready:[2, 0, 0, 9][1, 0, 0, 6]
	running:[3, 0] end burst, end process
************** TIME: 00000008 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 4]
************** TIME: 00000009 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 3]
************** TIME: 00000010 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 2]
************** TIME: 00000011 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 1]
************** TIME: 00000012 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 0] end burst, moved to ready
************** TIME: 00000013 **************
	waiting:
	ready:[2, 0, 0, 9]
	running:[1, 0] end burst, end process
************** TIME: 00000014 **************
	waiting:
	ready:
	running:[2, 4]
************** TIME: 00000015 **************
	waiting:
	ready:
	running:[2, 3]
************** TIME: 00000016 **************
	waiting:
	ready:
	running:[2, 2]
************** TIME: 00000017 **************
	waiting:
	ready:
	running:[2, 1]
************** TIME: 00000018 **************
	waiting:
	ready:
	running:[2, 0] end burst, moved to ready
************** TIME: 00000019 **************
	waiting:
	ready:
	running:[2, 3]
************** TIME: 00000020 **************
	waiting:
	ready:
	running:[2, 2]
************** TIME: 00000021 **************
	waiting:
	ready:
	running:[2, 1]
************** TIME: 00000022 **************
	waiting:
	ready:
	running:[2, 0] end burst, end process
CPU 0: 0.956522
Process 3:
Turnaround time: 3 	Waiting time: 2 	Response time: 1
Process 1:
Turnaround time: 13 	Waiting time: 3 	Response time: 0
Process 2:
Turnaround time: 21 	Waiting time: 13 	Response time: 12

Average CPU utilization: 0.956522
Average Turnaround Time: 12.333333
Average Waiting Time: 6.000000
Average Response Time: 4.333333
Throughput: 0.130435
