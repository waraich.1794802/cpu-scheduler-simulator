#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main (int argc, char** argv){
  srandom(time(NULL));

  int id = 10;
  int priority = 1;
  int affinity = -1;
  int arrival_time = 0;

  float ratio = 0.5;
  float scale = 20;
  float cicles = 8;

  if(argc<4){
    printf("usage <id> <ratio> <scale>\n");
    return 0;
  }

  id=atoi(argv[1]);
  ratio=atof(argv[2]);
  scale=atof(argv[3]);

  printf("PROCESS %d %d %d %d\n", id, priority, affinity, arrival_time);
  for(int i = 0; i<cicles; i++){
    if(i%2)
	printf("IO_BURST %d\n", abs((int)(2*scale*(1-ratio)+(float)(random()-RAND_MAX/2)/RAND_MAX*scale*0.5*(1-ratio))));
    else
    	printf("CPU_BURST %d\n", abs((int)(2*scale*ratio+(float)(random()-RAND_MAX/2)/RAND_MAX*scale*0.5*ratio)));
  }
  return 0;
}
