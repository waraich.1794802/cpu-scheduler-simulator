# Multi-CPU Scheduler Simulator
### What
"sched_sim" is an application that simulates a Multi-CPU scheduler. It supports 3 scheduling policies:
- Round Robin: runs the first process in the ready list, and once done the process is put at the end for said list. Since processes are put in the ready list in the order they come in, it also works as the First Come First Served policy if the scheduler simulator is set as preemptive.
- Priority: runs the process with highest priority first (where if pa < pb then pa has higher priority). Process also age every time they are not chosen, making them higher priority (to avoid starvation). Age is reset once they are chosen.
- Shortest Job First: runs the process whose burst is the shortest to take advantage of the convoy effect. It can be compared to a priority scheduler that uses the lenght of the next CPU as the priority parameter. I works as Shortest Remaining Job First in case of a preemptive scheduler.

Each processor can have its own different policy, and processes can have hard affinity to one of the processors, meaning they can only be run on that processor.

Once the application is run it prints out the state of the waiting, ready and running lists at every simulation step as well as other useful info like arrival times of the processes.

The waiting and running lists show the process id and remaining burst time ([id, time]), while the ready list also shows priority and affinity ([id, priority, affinity, time]).

At the end of the execution other useful measures are also printed out: cpu utilization, throughput, turnaround time, response time and waiting time.

### How
- "fake_os.c" contains the functions to create processes' pcbs and the function that gets called at every step (FakeOS_simStep). This function - manages the various lists (waiting and ready) and decreses the event timers as needed. Once that's done it checks if there are processors without processes running on them and calls the scheduler function for such processors. If no scheduling function is assigned then it uses the one for processor 0.
- "fake_process.c" contains the function to load processes from text files to the FakeProcess struct.
- "fake_schedulers.c" containts the actual implementation of the policies. It works by scanning the ready list and using pcb and event parameters (priority, affinity, length of next CPU burst) to determine the process to run, if any is eligible.
- "sched_sim.c" provides the main where it sets up the os, reads the inputed processes path files and contains the simulation loop.

### How to run
The configuration file is in "example/config.txt" by default but can be changed through the option -c:
```bash
./sched_sim -c myconfig.txt
```
Any subsequent -c will be ignored.
The main configuration file should be formatted as such:
```
PREEMPTIVE: p
PROCESSORS: n
SCHEDULING: a
SCHEDULING: b
.
.
.
SCHEDULING: c
```

p should be either 0 (false) or 1 (true). n is the number of processors and a,b,c are numbers identifying the scheduling policy for the corrisponding processor, starting from processor 0 (line 3), then 1 (line 4) and so on.

a=0 is Round Robin, a=1 is Shortest Job First and a=2 is Priority, but other algorithms can be added by the user.

The process files should be formatted as such:
```
PROCESS id priority affinity arrival_time
burst_type burst_duration
.
.
.
burst_type burst_duration
```

- id is a number to identify the process (different from pid which gets assigned by os!).
- arrival_time is the time the process "shows up" and get transferred to either waiting or ready list.
- priority and affinity are exactly that, with 0 being the minimum priority and -1 affinity meaning it can be run on any core.
- burst_type is either CPU_BURST or IO_BURST and burst_duration is a >0 integer equal to the length of the burst.

To actually run the program:
```bash
./sched_sim process_path1 process_path2 ... process_pathN
```
Where "process_pathX" is the path for the text document describing the process X.

To compile use the makefile.

### proc_gen
It's included in example/ a program to generate processes' profiles by giving in input id, ratio and scale. Ratio is the percentage of CPU bursts' total length over the combined length of all bursts. Scale is the average between the length of any pair of CPU and IO bursts. On top of this the program adds a little perturbation through a pseudo-random value normalized on the scale of the bursts. By default affinity is set to -1 and priority is set to 1.


```bash
./proc_gen <id> <ratio> <scale>
```
